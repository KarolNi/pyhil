#    pyHIL - app for testing autopilots
#    Copyright (C) 2014  KarolNi
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#v0.3
#import devpath
from pyjsbsim import FGFDMExec
from pylab import *
import time
from socket import *
import numpy
import struct
import thread

#EPBC lat52.267944,long20.9163
#mag field N,E,D  18,605.0 nT 	1,698.1 nT 	46,310.2 nT 

mag=[18605.0,1698.1,46310.2]
dt=0.005
visualisation_adress = ('10.42.0.12', 5500)
board_adress = ('127.0.0.1', 5501)



control_vector_norm = {'aileron':0, 'elevator':0, 'throttle':0, 'rudder':0, 'flap':0}
state_vector={'latitude_gps':0, 'longtitude_gps':0, 'altitude_gps':0, 'psi_dot':0, 'phi_dot':0, 'theta_dot':0, 'x_dot_dot':0, 'y_dot_dot':0, 'z_dot_dot':0, 'static_p':0, 'dynamic_p':0, 'B_x':0, 'B_y':0, 'B_z':0, 'psi_gps':0, 'V_gps':0}

def visualisationPrepare(fdm):
  string=(
    str(fdm.get_property_value("position/lat-gc-deg"))+','+#position
    str(fdm.get_property_value("position/long-gc-deg"))+','+
    str(fdm.get_property_value("position/h-sl-ft"))+','+
    str(fdm.get_property_value("attitude/roll-rad")*57.29578)+','+#attitude
    str(fdm.get_property_value("attitude/pitch-rad")*57.29578)+','+
    str(fdm.get_property_value("attitude/heading-true-rad")*57.29578)+','+
    str(fdm.get_property_value("/orientation/side-slip-deg")*57.29578)+','+#wrong name - don't work
    str(fdm.get_property_value("fcs/elevator-cmd-norm"))+','+#external visualisation
    str(fdm.get_property_value("fcs/flap-cmd-norm"))+','+
    str(fdm.get_property_value("fcs/aileron-cmd-norm"))+','+#left
    str(-fdm.get_property_value("fcs/aileron-cmd-norm"))+','+
    str(-fdm.get_property_value("fcs/rudder-cmd-norm"))+','+
    str(fdm.get_property_value("propulsion/engine/engine-rpm"))+','+#TODO
    str(fdm.get_property_value("fcs/throttle-cmd-norm[0]"))+','+#HUD input visualisation
    str(fdm.get_property_value("fcs/aileron-cmd-norm"))+','+
    str(fdm.get_property_value("fcs/elevator-cmd-norm"))+','+
    str(fdm.get_property_value("fcs/rudder-cmd-norm"))+'\n')
  return string
  
def controlsReciver(sock, vect):
  while(1):
    t=struct.unpack('<b', (sock.recv(1)))[0]
    #print(t.__class__.__name__)#":".join("{:02x}".format(ord(c)) for c in t))
    #print(t)
    if (t!=0x39):
   #   print('a')
      continue
      
    if (struct.unpack('<b', (sock.recv(1)))[0]!=0x1b):
    #  print('b')
      continue 
    
    buf=sock.recv(12)
    #print(":".join("{:02x}".format(ord(c)) for c in buf))
    try:
      pwm = list(struct.unpack('<6h', buf))
    except:
     # print ("struct")
      continue
    #print (pwm[0])
    vect["aileron"]  = (pwm[0]-1500)/500.0
    vect["elevator"] = (pwm[1]-1500)/500.0
    vect["throttle"] = (pwm[2]-1000)/1000.0
    vect["rudder"]   = (pwm[3]-1500)/500.0
    #vect["aux"]      = (pwm[4]-1500)/500.0
    #vect["aliRight"] = (pwm[5]-1500)/500.0
    print(vect)
 
  
def fdmInput(vect):
  fdm.set_property_value("fcs/aileron-cmd-norm",vect["aileron"])
  fdm.set_property_value("fcs/elevator-cmd-norm",vect["elevator"])
  fdm.set_property_value("fcs/rudder-cmd-norm",vect["rudder"])
  fdm.set_property_value("fcs/throttle-cmd-norm[0]",vect["throttle"])
  #fdm.set_property_value("fcs/flap-cmd-norm",flap)

class SensorsModel:  
  _i_gps=40
  _gps_samples_interval=40
  _t_lat
  _t_long
  _t_alt
  _t_psi
  _t_v
  _gps_flag=1 
  _state_vector
  _fdm
  
  def __init__(self, fdm, state_vector):
    self._fdm=fdm
    self._state_vector=state_vector
    
  def run(self)
    _gpsMpdel()
    _gyroModel()
    _accModel()
    _magModel()
    _aeroModel()
    return _state_vector
    
  def _gpsModel(self):#TODO 5Hz, lag, noise
    if _iGps==_gpsSamplesInterval:#limit updates to 5Hz
      _state_vector['latitude_gps']=_fdm.get_property_value("position/lat-gc-deg")
      _state_vector['longtitude_gps']=_fdm.get_property_value("position/long-gc-deg")
      _state_vector['altitude_gps']=_fdm.get_property_value("position/h-sl-ft")*0.3048 #meters
      _state_vector['psi_gps']=_fdm.get_property_value("attitude/heading-true-rad")*57.29578 #degries
      _state_vector['V_gps']=(_fdm.get_property_value("velocities/v-north-fps")**2+_fdm.get_property_value("velocities/v-east-fps")**2)**0.5*0.3048*_gps_flag#m/s
      _gps_flag=_gps_flag*(-1)#toggles V_gps sign to emphase new gps data
      _iGps=0
  
  def gyroModel(self,fdm, state_vector):
    _state_vector['phi_dot']=_fdm.get_property_value("velocities/phidot-rad_sec")*57.29578#deg
    _state_vector['psi_dot']=_fdm.get_property_value("velocities/psidot-rad_sec")*57.29578
    _state_vector['theta_dot']=_fdm.get_property_value("velocities/thetadot-rad_sec")*57.29578
  
  def accModel(self,fdm, state_vector):
    _state_vector['x_dot_dot']=_fdm.get_property_value("accelerations/a-pilot-x-ft_sec2")*0.3048#m/s^2
    _state_vector['y_dot_dot']=-_fdm.get_property_value("accelerations/a-pilot-y-ft_sec2")*0.3048
    _state_vector['z_dot_dot']=_fdm.get_property_value("accelerations/a-pilot-z-ft_sec2")*0.3048
  
  def magModel(self,fdm, state_vector):#TODO!!!!!!!!! rotation 
    mag
    _state_vector['B_x']=_fdm.get_property_value("attitude/phi-rad")#rad
    _state_vector['B_y']=_fdm.get_property_value("attitude/theta-rad")
    _state_vector['B_z']=_fdm.get_property_value("attitude/psi-rad")
  
  def aeroModel(self,fdm, state_vector):
    _state_vector['static_p']=_fdm.get_property_value("atmosphere/P-psf")*47.88#Pa
    _state_vector['dynamic_p']=_fdm.get_property_value("aero/qbar-psf")*47.88
 
#def fdmOutput(fdm,state_vector):  #Legacy
#  state_vector=gpsModel(fdm,state_vector)
#  state_vector=gyroModel(fdm,state_vector)
#  state_vector=accModel(fdm,state_vector)
#  state_vector=magModel(fdm,state_vector)
#  state_vector=aeroModel(fdm,state_vector)
  
def sendDataToBoard(vect,sock):
  buff=struct.pack('<I16f',    0x4c56414f, vect['latitude_gps'], vect['longtitude_gps'], vect['altitude_gps'], vect['psi_dot'], vect['phi_dot'], vect['theta_dot'], vect['x_dot_dot'], vect['y_dot_dot'], vect['z_dot_dot'], vect['static_p'], vect['dynamic_p'], vect['B_x'], vect['B_y'], vect['B_z'], vect['psi_gps'], vect['V_gps'])
  #buff=struct.pack('<I16f',    0x4c56414f, 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15)
  #print(":".join("{:02x}".format(ord(c)) for c in buff))
  
  sock.send(buff)
                         
  

# load
fdm = FGFDMExec()#debug_level=0)
#fdm.load_model("c172p")#tu154b")#pa28")
fdm.load_model("Rascal")

# Initial Conditions
fdm.set_property_value("ic/h-sl-ft",344.5+10000)
fdm.set_property_value("ic/lat-gc-deg",52.2705187)
fdm.set_property_value("ic/long-gc-deg",20.89796817)
fdm.set_property_value("ic/psi-true-deg",16.23)
fdm.set_property_value("ic/vc-kts",100)
fdm.set_property_value("ic/gamma-deg",10)
#fdm.do_trim(0)


#fdm.set_property_value("propulsion/engine[0]/set-running",1)



vis_sock = socket(AF_INET, SOCK_DGRAM)
vis_sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)#??????
#serversock.connect(ADDR)
board_sock = socket(AF_INET, SOCK_STREAM)
board_sock.connect(board_adress)######odkomentowac
board_sock.setblocking(1)

thread.start_new_thread(controlsReciver, (board_sock, control_vector_norm))

# run
fdm.set_dt(dt)
#fdm.set_property_value("fcs/elevator-cmd-norm",-1)
fdm.run_ic()
fdm.run()
fdm.set_property_value("propulsion/set-running",-1)# -1!!!!!!!
#print("running"+ str(fdm.get_property_value("propulsion/set-running")))
fdm.set_property_value("fcs/throttle-cmd-norm",float(1))
#print("thr"+ str(fdm.get_property_value("fcs/throttle-cmd-norm")))
fdm.set_property_value("fcs/mixture-cmd-norm",float(0.87))
#print("mix"+ str(fdm.get_property_value("fcs/mixture-cmd-norm")))

fdm.set_property_value("propulsion/magneto-cmd",int(1)) #don't work, but engine's running
#print("magneto"+ str(fdm.get_property_value("propulsion/engine/magneto-cmd"))) 
fdm.set_property_value("propulsion/starter-cmd",float(1))
#print("starter"+ str(fdm.get_property_value("propulsion/engine/starter-cmd")))
#
#
fdm.run()
#print("rpm"+ str(fdm.get_property_value("propulsion/engine/engine-rpm")))

sens=SensorsModel(fdm, state_vector)

timer=time.time()
while(1):
  fdmInput(control_vector_norm)
  if (fdm.run()==False):
    print ("Kaboom")
    break
  #print(fdm.get_sim_time())
  vis_sock.sendto(visualisationPrepare(fdm),visualisation_adress)
  state_vector=SensorsModel.run()
  sendDataToBoard(state_vector,board_sock)
  
  sleep_time=dt-(time.time()-timer)
  timer=time.time()
  if(sleep_time<0):
    sleep_time=0
  print (str(state_vector['theta_dot'])+" "+str(state_vector['phi_dot'])+" "+str(state_vector['psi_dot']))
  #print(state_vector)
  #print("rpm"+ str(fdm.get_property_value("propulsion/engine/engine-rpm")))
  time.sleep(sleep_time)

# simulate
#(t,y) = fdm.simulate(
#    t_final=50,
 #   dt=0.1,
  #  record_properties=["position/h-agl-ft", "attitude/theta-deg"])
#
#3 plot
#title("test")
#xla3bel("t, sec")
#ylabel("h-agl, ft")
#plot(t,y["position/h-agl-ft"])
#show()

